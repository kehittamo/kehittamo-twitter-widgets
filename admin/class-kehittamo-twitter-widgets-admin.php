<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://kehittamo.fi
 * @since      1.0.0
 *
 * @package    Kehittamo_Twitter_Widgets
 * @subpackage Kehittamo_Twitter_Widgets/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Kehittamo_Twitter_Widgets
 * @subpackage Kehittamo_Twitter_Widgets/admin
 * @author     Niko Myllynen <niko.myllynen@kehittamo.fi>
 */
class Kehittamo_Twitter_Widgets_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

 	/**
	 * Create our widgets
	 *
	 * @since    1.0.0
	 */
	public function register_widgets() {

	    register_widget( 'Kehittamo_Twitter_Widgets_Widget' );
	    register_widget( 'Kehittamo_Twitter_Widgets_Search_Widget' );
        //COLLECTION WIDGET NOT PRODUCTION READY!!!
		//register_widget( 'Kehittamo_Twitter_Widgets_Collection_Widget' );

	}

	/**
	 * Register the settings page
	 *
	 * @since    1.0.0
	 */
	public function add_admin_menu() {
	    add_options_page( 'Kehittamo Twitter Widgets', __('Twitter widgets', 'kehittamo-twitter-widgets'), 'manage_options', 'twitter-widgets', array($this, 'create_admin_interface'));
	}

	/**
	 * Callback function for the admin settings page.
	 *
	 * @since    1.0.0
	 */
	public function create_admin_interface(){

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/kehittamo-twitter-widgets-admin-display.php';

	}

	/**
	 * Creates our settings sections with fields etc.
	 *
	 * @since    1.0.0
	 */
	public function settings_api_init(){

		/*
		* SECTIONS
		*/

		// Add the Setup settings section to our plugins settings page
	 	add_settings_section(
			'kehittamo_twitter_widgets_general_settings_section',
			__('Setup Twitter Widgets', 'kehittamo-twitter-widgets'),
			array($this, 'setting_section_callback_function'),
			'twitter-widgets'
		);

		/*
		* SETTINGS FIELDS
		*/

		// Add the field for twitter consumer key
	 	add_settings_field(
			'kehittamo_twitter_widgets_consumer_key',
			__('Twitter Consumer Key:', 'kehittamo-twitter-widgets'),
			array($this, 'consumer_key_setting_callback_function'),
			'twitter-widgets',
			'kehittamo_twitter_widgets_general_settings_section'
		);

		// Add the field for twitter consumer secret
	 	add_settings_field(
			'kehittamo_twitter_widgets_consumer_secret',
			__('Twitter Consumer Secret:', 'kehittamo-twitter-widgets'),
			array($this, 'consumer_secret_setting_callback_function'),
			'twitter-widgets',
			'kehittamo_twitter_widgets_general_settings_section'
		);

		// Add the field for twitter access token
	 	add_settings_field(
			'kehittamo_twitter_widgets_access_token',
			__('Twitter Access Token:', 'kehittamo-twitter-widgets'),
			array($this, 'access_token_setting_callback_function'),
			'twitter-widgets',
			'kehittamo_twitter_widgets_general_settings_section'
		);

		// Add the field for twitter access token secret
	 	add_settings_field(
			'kehittamo_twitter_widgets_access_token_secret',
			__('Twitter Access Token Secret:', 'kehittamo-twitter-widgets'),
			array($this, 'access_token_secret_setting_callback_function'),
			'twitter-widgets',
			'kehittamo_twitter_widgets_general_settings_section'
		);

		// Add the field for transient duration
	 	add_settings_field(
			'kehittamo_twitter_widgets_transient_duration',
			__('Transient duration:', 'kehittamo-twitter-widgets'),
			array($this, 'transient_duration_setting_callback_function'),
			'twitter-widgets',
			'kehittamo_twitter_widgets_general_settings_section'
		);

		// Add a new setting to the options table
		register_setting( 'twitter-widgets', 'kehittamo_twitter_widgets_consumer_key' );
		register_setting( 'twitter-widgets', 'kehittamo_twitter_widgets_consumer_secret' );
		register_setting( 'twitter-widgets', 'kehittamo_twitter_widgets_access_token' );
		register_setting( 'twitter-widgets', 'kehittamo_twitter_widgets_access_token_secret' );
		register_setting( 'twitter-widgets', 'kehittamo_twitter_widgets_transient_duration' );

	}

	//Our callback functions

	/* SECTIONS */
	function setting_section_callback_function(){
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/kehittamo-twitter-widgets-section-display.php';
	}

	/* SETTINGS FIELDS */
	function consumer_key_setting_callback_function() {
 		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/kehittamo-twitter-widgets-consumer-key-display.php';
 	}

	function consumer_secret_setting_callback_function() {
 		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/kehittamo-twitter-widgets-consumer-secret-display.php';
 	}

 	function access_token_setting_callback_function() {
 		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/kehittamo-twitter-widgets-access-token-display.php';
 	}

 	function access_token_secret_setting_callback_function() {
 		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/kehittamo-twitter-widgets-access-token-secret-display.php';
 	}

 	function transient_duration_setting_callback_function() {
 		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/kehittamo-twitter-widgets-transient-duration-display.php';
 	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Kehittamo_Twitter_Widgets_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Kehittamo_Twitter_Widgets_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/kehittamo-twitter-widgets-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Kehittamo_Twitter_Widgets_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Kehittamo_Twitter_Widgets_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/kehittamo-twitter-widgets-admin.js', array( 'jquery' ), $this->version, false );

	}

}
