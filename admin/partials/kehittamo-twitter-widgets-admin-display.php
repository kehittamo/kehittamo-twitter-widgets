<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://kehittamo.fi
 * @since      1.0.0
 *
 * @package    Kehittamo_Twitter_Widgets
 * @subpackage Kehittamo_Twitter_Widgets/admin/partials
 */
?>

<div class="wrap">
    <?php screen_icon(); ?>
    <h2><?php _e('Kehittamo Twitter Widgets', 'kehittamo-twitter-widgets'); ?></h2>
    <form method="post" action="options.php">
        <?php settings_fields('twitter-widgets'); ?>
        <?php do_settings_sections('twitter-widgets'); ?>
        <?php submit_button('Save Changes'); ?>
    </form>
</div>
