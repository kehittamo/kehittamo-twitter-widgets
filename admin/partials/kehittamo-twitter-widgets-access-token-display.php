<?php

/**
 * The setting for access token
 *
 * This file is used to setup a settings field
 *
 * @link       http://kehittamo.fi
 * @since      1.0.0
 *
 * @package    Kehittamo_Twitter_Widgets
 * @subpackage Kehittamo_Twitter_Widgets/admin/partials
 */
?>

<?php
$access_token = get_option('kehittamo_twitter_widgets_access_token');
?>
<p><label for="access-token">
	<input type="text" value="<?php echo $access_token; ?>" id="access-token" name="kehittamo_twitter_widgets_access_token" />
</label></p>
