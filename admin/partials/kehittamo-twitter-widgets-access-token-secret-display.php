<?php

/**
 * The setting for access token secret
 *
 * This file is used to setup a settings field
 *
 * @link       http://kehittamo.fi
 * @since      1.0.0
 *
 * @package    Kehittamo_Twitter_Widgets
 * @subpackage Kehittamo_Twitter_Widgets/admin/partials
 */
?>

<?php
$access_token_secret = get_option('kehittamo_twitter_widgets_access_token_secret');
?>
<p><label for="access-token-secret">
	<input type="text" value="<?php echo $access_token_secret; ?>" id="access-token-secret" name="kehittamo_twitter_widgets_access_token_secret" />
</label></p>
