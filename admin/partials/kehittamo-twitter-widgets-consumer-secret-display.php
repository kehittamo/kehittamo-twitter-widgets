<?php

/**
 * The setting for consumer secret
 *
 * This file is used to setup a settings field
 *
 * @link       http://kehittamo.fi
 * @since      1.0.0
 *
 * @package    Kehittamo_Twitter_Widgets
 * @subpackage Kehittamo_Twitter_Widgets/admin/partials
 */
?>

<?php
$consumer_secret = get_option('kehittamo_twitter_widgets_consumer_secret');
?>
<p><label for="consumer-secret">
	<input type="text" value="<?php echo $consumer_secret; ?>" id="consumer-secret" name="kehittamo_twitter_widgets_consumer_secret" />
</label></p>
