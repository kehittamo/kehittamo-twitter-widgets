<?php

/**
 * The setting for transient duration
 *
 * This file is used to setup a settings field
 *
 * @link       http://kehittamo.fi
 * @since      1.0.0
 *
 * @package    Kehittamo_Twitter_Widgets
 * @subpackage Kehittamo_Twitter_Widgets/admin/partials
 */
?>

<?php
$transient_duration = get_option('kehittamo_twitter_widgets', 30);
?>
<p><label for="transient-duration">
	<input type="number" value="<?php echo $transient_duration; ?>" id="transient-duration" name="kehittamo_twitter_widgets_transient_duration" /> <?php _e('minutes', 'kehittamo-twitter-widgets'); ?>
</label></p>
