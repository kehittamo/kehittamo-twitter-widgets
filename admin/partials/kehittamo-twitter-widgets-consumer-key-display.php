<?php

/**
 * The setting for consumer key
 *
 * This file is used to setup a settings field
 *
 * @link       http://kehittamo.fi
 * @since      1.0.0
 *
 * @package    Kehittamo_Twitter_Widgets
 * @subpackage Kehittamo_Twitter_Widgets/admin/partials
 */
?>

<?php
$consumer_key = get_option('kehittamo_twitter_widgets_consumer_key');
?>
<p><label for="consumer-key">
	<input type="text" value="<?php echo $consumer_key; ?>" id="transient-duration" name="kehittamo_twitter_widgets_consumer_key" />
</label></p>
