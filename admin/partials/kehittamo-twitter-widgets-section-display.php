<?php

/**
 * The setup settings section in the settings page
 *
 * This file is used to setup a settings section
 *
 * @link       http://kehittamo.fi
 * @since      1.0.0
 *
 * @package    Kehittamo_Twitter_Widgets
 * @subpackage Kehittamo_Twitter_Widgets/admin/partials
 */
?>
