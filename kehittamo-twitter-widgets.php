<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://kehittamo.fi
 * @since             1.0.0
 * @package           Kehittamo_Twitter_Widgets
 *
 * @wordpress-plugin
 * Plugin Name:       Kehittämö Twitter Widgets
 * Plugin URI:        http://kehittamo.fi
 * Description:       A collection of Twitter widgets.
 * Version:           1.0.0
 * Author:            Niko Myllynen
 * Author URI:        http://kehittamo.fi
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       kehittamo-twitter-widgets
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-kehittamo-twitter-widgets-activator.php
 */
function activate_kehittamo_twitter_widgets() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-kehittamo-twitter-widgets-activator.php';
	Kehittamo_Twitter_Widgets_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-kehittamo-twitter-widgets-deactivator.php
 */
function deactivate_kehittamo_twitter_widgets() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-kehittamo-twitter-widgets-deactivator.php';
	Kehittamo_Twitter_Widgets_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_kehittamo_twitter_widgets' );
register_deactivation_hook( __FILE__, 'deactivate_kehittamo_twitter_widgets' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-kehittamo-twitter-widgets.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_kehittamo_twitter_widgets() {

	$plugin = new Kehittamo_Twitter_Widgets();
	$plugin->run();

}
run_kehittamo_twitter_widgets();
