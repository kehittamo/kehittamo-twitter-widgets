<?php
/**
 * Main widget
 *
 *
 * @package    Kehittamo_Twitter_Widgets
 * @subpackage Kehittamo_Twitter_Widgets/widget
 * @author     Niko Myllynen <niko.myllynen@kehittamo.fi>
 */
class Kehittamo_Twitter_Widgets_Widget extends WP_Widget {


 	/**
	 * Constructor for the widget
	 *
	 * @since    1.0.0
	 */
    public function __construct() {
        parent::__construct(
            'kehittamo-twitter-widgets-widget', // Base ID
            'Twitter widgets list', // Name
            array(
                'description' => __( 'Add tweet list to sidebar', 'kehittamo-twitter-widgets' ),
                'classname' => 'kehittamo-twitter-widgets-widget'
            ) // Args
        );
    }

    /**
	 * Admin form in the widget area
	 *
	 * @since    1.0.0
	 */
    public function form( $instance ) {

    	$topic = strip_tags($instance['topic']);
        $slug = strip_tags($instance['slug']);
        $owner_screen_name = strip_tags($instance['owner_screen_name']);
        $count = strip_tags($instance['count']);
    	?>
		<p>
			<label for="<?php echo $this->get_field_id('topic'); ?>"><?php _e('Topic:'); ?>
            <span class="description"><?php _e('List topic.', 'kehittamo-twitter-widgets'); ?></span>
            </label>
			<input class="widefat" id="<?php echo $this->get_field_id('topic'); ?>" name="<?php echo $this->get_field_name('topic'); ?>" type="text" value="<?php echo esc_attr($topic); ?>" />
		</p>
        <p>
            <label for="<?php echo $this->get_field_id('slug'); ?>"><?php _e('List slug:'); ?>
            <span class="description"><?php _e('i.e. sdp-n-vaikuttajat', 'kehittamo-twitter-widgets'); ?></span>
            </label>
            <input class="widefat" id="<?php echo $this->get_field_id('slug'); ?>" name="<?php echo $this->get_field_name('slug'); ?>" type="text" value="<?php echo esc_attr($slug); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('owner_screen_name'); ?>"><?php _e('List owner screen name:'); ?>
            <span class="description"><?php _e('i.e. demari_fi', 'kehittamo-twitter-widgets'); ?></span>
            </label>
            <input class="widefat" id="<?php echo $this->get_field_id('owner_screen_name'); ?>" name="<?php echo $this->get_field_name('owner_screen_name'); ?>" type="text" value="<?php echo esc_attr($owner_screen_name); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('count'); ?>"><?php _e('Tweet count:'); ?>
            <span class="description"><?php _e('How many latest list tweets are shown', 'kehittamo-twitter-widgets'); ?></span>
            </label>
            <input class="widefat" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>" type="number" value="<?php echo esc_attr($count); ?>" />
        </p>
    	<?php
    }

	/**
	 * Update function for the widget
	 *
	 * @since    1.0.0
	 */
    public function update( $new_instance, $old_instance ) {
        // processes widget options to be saved, deletes transient cache
        $instance = $old_instance;
        delete_transient( strip_tags($instance['slug']) );
        $instance['topic'] = strip_tags($new_instance['topic']);
        $instance['slug'] = strip_tags($new_instance['slug']);
        $instance['owner_screen_name'] = strip_tags($new_instance['owner_screen_name']);
        $instance['count'] = strip_tags($new_instance['count']);
        return $instance;
    }

    /**
     * Relative time for tweets by Chris, modified version
     *
     * @since    1.0.0
     * @access   private
     */
    private function pretty_relative_time($time) {
        if ($time !== intval($time)) { $time = strtotime($time); }
        $d = time() - $time;
        if ($d >= 60*60*24) { return intval($d / (60*60*24)) . "pv"; }
        if ($d >= 60*60) { return intval($d / (60*60)) . "h"; }
        if ($d >= 60) { return intval($d / 60) . "m"; }
        if ($d >= 0) { return intval($d) . "s"; }
    }

    /**
     * Replace hashtags and links with links in tweet text
     *
     * @since 1.0.0
     * @access private
     */
    private function twitterify($ret) {
        $ret = preg_replace("#(^|[\n ])([\w]+?://[\w]+[^ \"\n\r\t< ]*)#", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $ret);
        $ret = preg_replace("#(^|[\n ])((www|ftp)\.[^ \"\t\n\r< ]*)#", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);
        $ret = preg_replace("/@(\w+)/u", "<a href=\"http://www.twitter.com/\\1\" target=\"_blank\">@\\1</a>", $ret);
        $ret = preg_replace("/#(\w+)/u", "<a href=\"http://twitter.com/search?q=\\1\" target=\"_blank\">#\\1</a>", $ret);
        return $ret;
    }

	/**
	 * Outputs the widget with the selected settings
	 *
	 * @since    1.0.0
	 */
    public function widget( $args, $instance ) {

    	extract($args);
        $topic = strip_tags($instance['topic']);
        $slug = strip_tags($instance['slug']);
        $owner_screen_name = strip_tags($instance['owner_screen_name']);
        $count = strip_tags($instance['count']);

        // Get any existing copy of our transient data with the name of our list slug
        if ( false === ( $content = get_transient( $slug ) ) ) {
            // It wasn't there, so regenerate the data and save the transient
            $consumer_key = get_option('kehittamo_twitter_widgets_consumer_key');
            $consumer_secret = get_option('kehittamo_twitter_widgets_consumer_secret');
            $access_token = get_option('kehittamo_twitter_widgets_access_token');
            $access_token_secret = get_option('kehittamo_twitter_widgets_access_token_secret');
            $transient_duration = get_option('kehittamo_twitter_widgets_transient_duration');
            // Use Abraham's TwitterOAuth
            $connection = new Abraham\TwitterOAuth\TwitterOAuth($consumer_key, $consumer_secret, $access_token, $access_token_secret);
            // Lists require slug and owner screen name for retrieval. Disregard retweets and request tweet entities such as hashtags and links for formatting
            $content = $connection->get("lists/statuses", array('slug' => $slug, 'owner_screen_name' => $owner_screen_name, 'count' => $count, 'include_rts' => false, 'tweet_entities' => 1));
            // Check if request was successful
            if ($connection->getLastHttpCode() == 200) {
                set_transient( $slug, $content, 60 * $transient_duration );
            } else {
                // Handle error case
            }
        }

    	/*
	    * The content of the widget
	    */
        echo $before_widget;
        echo '<h1 class="kehittamo-twitter-widgets-topic">' . $topic . '</h1>';
        echo '<div class="kehittamo-twitter-widgets-tweets">';
            foreach ($content as $tweet) {
                echo '<div class="kehittamo-twitter-widgets-tweet">';
                    echo '<img class="kehittamo-twitter-widgets-profile-image-url" src="' . $tweet->user->profile_image_url_https . '" />';
                    echo '<p class="kehittamo-twitter-widgets-date">' . $this->pretty_relative_time((str_replace("+0000", "", $tweet->created_at))) . '</p>';
                    echo '<h2 class="kehittamo-twitter-widgets-name">' . $tweet->user->name . ' <span class="kehittamo-twitter-widgets-screen-name">' . $this->twitterify('@' . $tweet->user->screen_name) . '</span></h2>';
                    echo '<p class="kehittamo-twitter-widgets-text">' . $this->twitterify($tweet->text) . '</p>';
                    echo '<div class="kehittamo-twitter-widgets-buttons">';
                        echo '<a href="https://twitter.com/intent/tweet?in_reply_to=' . $tweet->id . '"><div class="kehittamo-twitter-widgets-buttons-reply"></div></a>';
                        echo '<a href="https://twitter.com/intent/retweet?tweet_id=' . $tweet->id . '"><div class="kehittamo-twitter-widgets-buttons-retweet"></div></a>';
                        echo '<a href="https://twitter.com/intent/favorite?tweet_id=' . $tweet->id . '"><div class="kehittamo-twitter-widgets-buttons-star"></div></a>';
                    echo '</div>';
                echo '</div>';
            }
            //echo '<div class="kehittamo-twitter-widgets-loadmore">' . __("Load more", "kehittamo-twitter-widgets") . '</div>';
        echo '</div>';

		echo $after_widget;
    }
}
?>
