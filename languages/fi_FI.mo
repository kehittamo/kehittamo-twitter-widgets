��          �            h     i  %   �     �     �  
   �     �     �                     -     ;     J     a  �  i     :  ,   V     �     �     �     �     �     �                '     6     F  	   ^               	                                                         
        Add tweet topic to sidebar How many latest list tweets are shown Kehittamo Twitter Lists List owner screen name: List slug: List topic. Setup Twitter Lists Topic: Transient duration: Tweet count: Twitter lists i.e. demari_fi i.e. sdp-n-vaikuttajat minutes Project-Id-Version: Kehittamo-Twitter-Lists
POT-Creation-Date: 2015-05-27 20:13+0200
PO-Revision-Date: 2015-05-27 20:16+0200
Last-Translator: Niko Myllynen <niko.myllynen@kehittamo.fi>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.7.4
X-Poedit-Basepath: ../
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e;esc_attr_e
Language: fi_FI
X-Poedit-SearchPath-0: .
 Lisää luettelo vimpaimeen Kuinka monta uusinta twiittiä näytetään. Kehittämö Twitter Luettelot Listan omistajan näyttönimi Listan slug: Listan otsikko. Twitter Listojen asetukset Otsikko: Välimuistin kesto Twiittien määrä: Twitter Listat esim. demari_fi esim. sdp-n-vaikuttajat minuuttia 