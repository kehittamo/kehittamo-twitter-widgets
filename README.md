# Kehittamö Twitter Widgets

The plugin enables the use of a Twitter List widget and a Twitter Search widget. A third Twitter Collection widget is not production ready.

## Setup

After activation, enter Twitter credentials on the Twitter widgets settings page. Also set the transient duration for all tweets fetched from Twitter.

## Additional information

The plugin has been constructed with the Wordpress Plugin Boilerplate.
